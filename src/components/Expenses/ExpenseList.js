import React from 'react';

// import './Styles/ExpenseList.css';
import ExpenseItem from './ExpenseItem';
import Card from '../Core/Card';

const ExpenseList = (props) => {
    if(props.items.length === 0) {
        return <h2 className='expense-list__fallback'>Found No Expenses</h2>
    }


    return (
        <Card className="expense-list">
            { props.items.map( item => <ExpenseItem
                key={item.id}
                title={item.title}
                date={item.date}
                amount={item.amount}
            />) }
        </Card>
    );
}

export default ExpenseList;
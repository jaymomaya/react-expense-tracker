import React from 'react';

// import './Styles/ExpenseDate.css';

/**
 *
 * @param  props
 * @returns  {JSX.Element}
 * @constructor
 *
 * props should come with a date object in it. This object should be of type Date.
 */

const ExpenseDate = (props) => {
    const month = props.date.toLocaleString('en-US', { month: 'long' });
    const day = props.date.toLocaleString('en-US', { day: '2-digit' });
    const year = props.date.getFullYear();

    return (
        <div className='expense-date'>
            <div className='expense-date__month'>{month}</div>
            <div className='expense-date__year'>{year}</div>
            <div className='expense-date__day'>{day}</div>
        </div>
    );
};

export default ExpenseDate;
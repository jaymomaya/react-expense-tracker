import React from 'react';

import ExpenseDate from './ExpenseDate';
import Card from '../Core/Card';
// import './Styles/ExpenseItem.css';

/**
 *
 * @param  props
 * @returns  {JSX.Element}
 * @constructor
 *
 * props should get 3 keys: 'date' which should be Date object, 'title' which should be String and 'amount' which should be any number
 */
const ExpenseItem = (props) => {
    return (
        <Card className='expense-item'>
            <ExpenseDate date={props.date} />
            <div className='expense-item__description'>
                <h2>{props.title}</h2>
                <div className='expense-item__price'>Rs. {props.amount}</div>
            </div>
        </Card>
    );
}

export default ExpenseItem;
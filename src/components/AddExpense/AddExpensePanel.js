import React from 'react';

import ExpenseForm from './ExpenseForm';
// import './Styles/AddExpensePanel.css';
import Card from "../Core/Card";

/**
 * This component will act as parent component to the Form component. But as our root component is App component, it will be the responsibility of App component to communicate with the server. So whenever the user presses the submit button of the form, it's data has to be passed to its parent which is the Panel component which we are making, in turn Panel will pass the data to its parent which will then be propagated to server at one point.
 * @param  props
 * @returns  {JSX.Element}
 * @constructor
 */
const AddExpensePanel = (props) => {
    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        };
        props.onAddExpense(expenseData);
    };

    return (
        <Card className='new-expense'>
            <ExpenseForm onSaveExpenseData={saveExpenseDataHandler} />
        </Card>
    );
};

export default AddExpensePanel;

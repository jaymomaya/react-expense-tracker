import {useState} from "react";
import Expenses from "./components/Expenses/Expenses";
import AddExpensePanel from "./components/AddExpense/AddExpensePanel";

const DUMMY_EXPENSES  = [
	{ id: 'e1', title: 'Some Item 1', amount: 500, date: new Date(2021, 11, 14), },
	{ id: 'e2', title: 'New TV', amount: 15000, date: new Date(2021, 2, 12) },
	{ id: 'e3', title: 'Life Insuarance', amount: 36000, date: new Date(2021, 10, 28), },
	{ id: 'e4', title: 'New Keyboard', amount: 1250, date: new Date(2022, 11, 12), },
];

function App() {
	const [expenses, setExpenses] = useState(DUMMY_EXPENSES);
    
	const addExpenseHandler = expense => {
        setExpenses( previousExpenses => [expense, ...previousExpenses]);
    }

	return (
		<div>
			<h2>Expense Tracker!</h2>
			<AddExpensePanel onAddExpense={addExpenseHandler}/>
			<Expenses 
				items={expenses}
			/>
			
		</div>
	);
}

export default App;